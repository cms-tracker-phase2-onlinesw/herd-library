#ifndef __SWATCH_CORE_RULES_PATHEXISTS_HXX__
#define __SWATCH_CORE_RULES_PATHEXISTS_HXX__


#include <ostream>

#include <boost/filesystem.hpp>


namespace swatch {
namespace core {
namespace rules {

template <typename T>
Match PathExists<T>::verify(const T& aValue) const
{
  if (this->isEmpty<T>(aValue)) {
    return Match(false, "Empty path");
  }

  boost::filesystem::path lPath = boost::filesystem::path(aValue);

  if (!mPrefix.empty())
    lPath = boost::filesystem::path(mPrefix) / lPath;

  if (!mExtension.empty())
    lPath.replace_extension(mExtension);

  return (boost::filesystem::exists(lPath));
}


template <typename T>
void PathExists<T>::describe(std::ostream& aStream) const
{
  aStream << "exists(x)";
}

} // namespace rules
} // namespace core
} // namespace swatch


#endif /* __SWATCH_CORE_RULES_PATHEXISTS_HXX__ */