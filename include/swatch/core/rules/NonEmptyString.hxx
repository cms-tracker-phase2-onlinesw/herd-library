#ifndef __SWATCH_CORE_RULES_NONEMPTYSTRING_HXX__
#define __SWATCH_CORE_RULES_NONEMPTYSTRING_HXX__


#include <ostream>


namespace swatch {
namespace core {
namespace rules {

template <typename T>
Match NonEmptyString<T>::verify(const T& aValue) const
{
  return verify<T>(aValue);
}


template <typename T>
void NonEmptyString<T>::describe(std::ostream& aStream) const
{
  aStream << "!x.empty()";
}

} // namespace rules
} // namespace core
} // namespace swatch


#endif /* __SWATCH_CORE_RULES_NONEMPTYSTRING_HXX__ */