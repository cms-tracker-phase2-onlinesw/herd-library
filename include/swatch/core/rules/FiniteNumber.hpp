#ifndef __SWATCH_CORE_RULES_FINITENUMBER_HPP__
#define __SWATCH_CORE_RULES_FINITENUMBER_HPP__


#include <cmath>

#include "swatch/core/Rule.hpp"


namespace xdata {
class Serializable;
}

namespace swatch {
namespace core {
namespace rules {

template <typename T>
class FiniteNumber : public Rule<T> {

public:
  FiniteNumber() {}
  virtual ~FiniteNumber() {}

  Match verify(const T& aValue) const
  {
    return verify<T>(aValue);
  }

private:
  template <typename U = T, typename std::enable_if<std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  Match verify(const U& aValue) const
  {
    const U& lValue = dynamic_cast<const U&>(aValue);
    return Match(lValue.isFinite());
  }

  template <typename U = T, typename std::enable_if<!std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  Match verify(const U& aValue) const
  {
    return Match(std::isfinite(aValue));
  }

  virtual void describe(std::ostream& aStream) const;
};

} // namespace rules
} // namespace core
} // namespace swatch


#include "swatch/core/rules/FiniteNumber.hxx"


#endif /* __SWATCH_CORE_RULES_FINITENUMBER_HPP__ */