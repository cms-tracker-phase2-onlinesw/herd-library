#ifndef __SWATCH_CORE_RULES_OUTOFRANGE_HXX__
#define __SWATCH_CORE_RULES_OUTOFRANGE_HXX__


#include <ostream>
#include <sstream>

#include "swatch/core/exception.hpp"


namespace swatch {
namespace core {
namespace rules {

template <typename T>
OutOfRange<T>::OutOfRange(const T& aLowerBound, const T& aUpperBound) :
  mLowerBound(aLowerBound),
  mUpperBound(aUpperBound)
{
  if (mLowerBound >= mUpperBound) {
    std::ostringstream lMsg;
    lMsg << "Upper bound (" << mUpperBound << ") is smaller than lower bound (" << mLowerBound << ")";
    SWATCH_THROW(RuleArgumentError(lMsg.str()));
  }
}


template <typename T>
Match OutOfRange<T>::verify(const T& aValue) const
{
  return Match(aValue < mLowerBound || mUpperBound < aValue);
}


template <typename T>
void OutOfRange<T>::describe(std::ostream& aStream) const
{
  aStream << "(x < " << mLowerBound << " || x > " << mUpperBound << ")";
}


} // namespace rules
} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_RULES_OUTOFRANGE_HXX__ */