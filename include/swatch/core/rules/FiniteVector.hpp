#ifndef __SWATCH_CORE_RULES_FINITEVECTOR_HPP__
#define __SWATCH_CORE_RULES_FINITEVECTOR_HPP__


#include <cmath>

#include "swatch/core/Rule.hpp"


namespace xdata {
class Serializable;
}

namespace swatch {
namespace core {
namespace rules {

//! Class for finite vector rule.
template <typename T>
class FiniteVector : public Rule<T> {

public:
  FiniteVector() {};
  virtual ~FiniteVector() {};

  /**
   * @brief      Checks if all elements in user-supplied vector are finite
   *
   * @param[in]  aValue  The vector to check
   *
   * @return     True if all elements in aValue are finite; false otherwise
   */
  virtual Match verify(const T& aValue) const;

  template <typename U = T, typename std::enable_if<std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  Match verify(const U& aValue) const
  {
    const U& lValue = dynamic_cast<const U&>(aValue);
    return { std::all_of(
                 lValue.begin(), lValue.end(),
                 [](const typename U::value_type& aX) { return aX.isFinite(); }),
             "" };
  }


  template <typename U = T, typename std::enable_if<!std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  Match verify(const U& aValue) const
  {
    return { std::all_of(
                 aValue.begin(), aValue.end(),
                 [](const typename U::value_type& aX) { return std::isfinite(aX); }),
             "" };
  }

private:
  virtual void describe(std::ostream& aStream) const;
};

} // namespace rules
} // namespace core
} // namespace swatch


#include "swatch/core/rules/FiniteVector.hxx"


#endif /* __SWATCH_CORE_RULES_FINITEVECTOR_HPP__ */