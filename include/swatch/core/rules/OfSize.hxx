#ifndef __SWATCH_CORE_RULES_OFSIZE_HXX__
#define __SWATCH_CORE_RULES_OFSIZE_HXX__


#include <ostream>


namespace swatch {
namespace core {
namespace rules {

template <typename T>
Match OfSize<T>::verify(const T& aValue) const
{
  return aValue.size() == mSize;
}


template <typename T>
void OfSize<T>::describe(std::ostream& aStream) const
{
  aStream << "len(x) = " << mSize;
}

} // namespace rules
} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_RULES_OFSIZE_HXX__ */