#ifndef __SWATCH_CORE_RULES_GREATERTHAN_HXX__
#define __SWATCH_CORE_RULES_GREATERTHAN_HXX__


#include <ostream>


namespace swatch {
namespace core {
namespace rules {

template <typename T>
Match GreaterThan<T>::verify(const T& aValue) const
{
  return Match(aValue > lLowerBound);
}


template <typename T>
void GreaterThan<T>::describe(std::ostream& aStream) const
{
  aStream << "x > " << lLowerBound;
}

} // namespace rules
} // namespace core
} // namespace swatch


#endif /* __SWATCH_CORE_RULES_GREATERTHAN_HXX__ */
