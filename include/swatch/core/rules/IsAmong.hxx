#ifndef __SWATCH_CORE_RULES_ISAMONG_HXX__
#define __SWATCH_CORE_RULES_ISAMONG_HXX__


#include <algorithm>


namespace swatch {
namespace core {
namespace rules {

template <typename T>
Match IsAmong<T>::verify(const T& aValue) const
{
  return verify<T>(aValue);
}


template <typename T>
void IsAmong<T>::describe(std::ostream& aStream) const
{
  return describe<T>(aStream);
}

} // namespace rules
} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_RULES_ISAMONG_HXX__ */