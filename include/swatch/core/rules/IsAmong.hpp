#ifndef __SWATCH_CORE_RULES_ISAMONG_HPP__
#define __SWATCH_CORE_RULES_ISAMONG_HPP__


#include "swatch/core/Rule.hpp"


namespace xdata {
class Serializable;
}

namespace swatch {
namespace core {
namespace rules {

template <typename T>
class IsAmong : public Rule<T> {

public:
  IsAmong(const std::vector<T>& aChoices) :
    mChoices(aChoices)
  {
  }
  ~IsAmong() {};

  virtual Match verify(const T& aValue) const;

private:
  template <typename U = T, typename std::enable_if<std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  Match verify(const U& aValue) const
  {
    return { std::find_if(mChoices.begin(), mChoices.end(), [aValue](const T& x) { return x.equals(aValue); }) != mChoices.end(), "" };
  }

  template <typename U = T, typename std::enable_if<!std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  Match verify(const U& aValue) const
  {
    return { std::find(mChoices.begin(), mChoices.end(), aValue) != mChoices.end(), "" };
  }

  virtual void describe(std::ostream& aStream) const;

  template <typename U = T, typename std::enable_if<std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  void describe(std::ostream& aStream) const
  {
    aStream << "x in {";
    for (auto lIt = mChoices.begin(); lIt != mChoices.end(); lIt++) {
      aStream << lIt->toString();
      if (lIt != (mChoices.end() - 1))
        aStream << ", ";
    }
    aStream << "}";
  }

  template <typename U = T, typename std::enable_if<!std::is_base_of<xdata::Serializable, U>::value>::type* = nullptr>
  void describe(std::ostream& aStream) const
  {
    aStream << "x in {";
    for (auto lIt = mChoices.begin(); lIt != mChoices.end(); lIt++) {
      aStream << *lIt;
      if (lIt != (mChoices.end() - 1))
        aStream << ", ";
    }
    aStream << "}";
  }

  std::vector<T> mChoices;
};

} // namespace rules
} // namespace core
} // namespace swatch


#include "swatch/core/rules/IsAmong.hxx"


#endif /* __SWATCH_CORE_RULES_ISAMONG_HPP__ */