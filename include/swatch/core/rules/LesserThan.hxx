#ifndef __SWATCH_CORE_RULES_LESSERTHAN_HXX__
#define __SWATCH_CORE_RULES_LESSERTHAN_HXX__


#include <ostream>


namespace swatch {
namespace core {
namespace rules {

template <typename T>
Match LesserThan<T>::verify(const T& aValue) const
{
  return Match(aValue < lLowerBound);
}


template <typename T>
void LesserThan<T>::describe(std::ostream& aStream) const
{
  aStream << "x < " << lLowerBound;
}

} // namespace rules
} // namespace core
} // namespace swatch


#endif /* __SWATCH_CORE_RULES_LESSERTHAN_HXX__ */