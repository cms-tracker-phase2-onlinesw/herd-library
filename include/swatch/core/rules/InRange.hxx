#ifndef __SWATCH_CORE_RULES_INRANGE_HXX__
#define __SWATCH_CORE_RULES_INRANGE_HXX__


#include <ostream>
#include <sstream>

#include "swatch/core/exception.hpp"


namespace swatch {
namespace core {
namespace rules {

template <typename T>
InRange<T>::InRange(const T& aLowerBound, const T& aUpperBound) :
  mLowerBound(aLowerBound),
  mUpperBound(aUpperBound)
{

  if (mLowerBound >= mUpperBound) {
    std::ostringstream lMsg;
    lMsg << "Upper bound (" << mUpperBound << ") is smaller than lower bound (" << mLowerBound << ")";
    SWATCH_THROW(RuleArgumentError(lMsg.str()));
  }
}


template <typename T>
Match InRange<T>::verify(const T& aValue) const
{
  return Match(mLowerBound < aValue && aValue < mUpperBound);
}


template <typename T>
void InRange<T>::describe(std::ostream& aStream) const
{
  aStream << mLowerBound << " < x < " << mUpperBound;
}

} // namespace rules
} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_RULES_INRANGE_HXX__ */