/**
 * @file    exception.hpp
 * @author  Alessandro Thea
 * @date    July 2014
 */

#ifndef __SWATCH_CORE_EXCEPTION_HPP__
#define __SWATCH_CORE_EXCEPTION_HPP__


// Standard headers
#include <exception>
#include <string>
#include <thread>

#include "boost/exception/diagnostic_information.hpp"
#include "boost/exception/exception.hpp"
#include "boost/throw_exception.hpp"


#define SWATCH_DEFINE_EXCEPTION(EXCEPTION_NAME)             \
  class EXCEPTION_NAME : public ::swatch::core::Exception { \
  public:                                                   \
    EXCEPTION_NAME(const std::string& aMessage) :           \
      ::swatch::core::Exception(aMessage)                   \
    {                                                       \
    }                                                       \
  };


#define SWATCH_THROW(X) \
  throw X << ::boost::throw_function(BOOST_CURRENT_FUNCTION) << ::boost::throw_file(__FILE__) << ::boost::throw_line((int)__LINE__)


namespace swatch {
namespace core {

class Exception : public virtual boost::exception, public virtual std::exception {

public:
  Exception(const std::string& aMessage);

  virtual ~Exception() throw();

  virtual const char* what() const throw();

private:
  std::string mMessage;
};


SWATCH_DEFINE_EXCEPTION(RuntimeError)
SWATCH_DEFINE_EXCEPTION(InvalidArgument)

} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_EXCEPTION_HPP__ */
