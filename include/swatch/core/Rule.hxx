#ifndef __SWATCH_CORE_RULE_HXX__
#define __SWATCH_CORE_RULE_HXX__


#include "Rule.hpp"

#include "swatch/core/utilities.hpp"


namespace swatch {
namespace core {

// ----------------------------------------------------------------------------
template <typename T>
const std::type_info& Rule<T>::type() const
{
  return typeid(T);
};
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
template <typename T>
Match Rule<T>::operator()(const boost::any& aValue) const
{
  const T* lValue = boost::any_cast<const T>(&aValue);

  if (lValue == NULL)
    SWATCH_THROW(RuleTypeMismatch("Mismatch between expected type (" + demangleName(typeid(T).name()) + ") and received type (" + demangleName(aValue.type().name()) + ")."));

  return verify(*lValue);
}
// ----------------------------------------------------------------------------


} // namespace core
} // namespace swatch

#endif /* __SWATCH_CORE_RULE_HXX__ */