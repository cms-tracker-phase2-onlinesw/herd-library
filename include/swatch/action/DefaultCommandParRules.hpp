#ifndef __SWATCH_ACTION_DEFAULTCOMMANDPARRULES_HPP__
#define __SWATCH_ACTION_DEFAULTCOMMANDPARRULES_HPP__


#include "swatch/core/Rule.hpp"
#include "swatch/core/rules/FiniteNumber.hpp"
#include "swatch/core/rules/FiniteVector.hpp"
#include "swatch/core/rules/None.hpp"


// Forward declarations
namespace xdata {

template <class T>
class BooleanType;
typedef BooleanType<bool> Boolean;

template <class T>
class DoubleType;
typedef DoubleType<double> Double;

template <class T>
class FloatType;
typedef FloatType<float> Float;

template <class T>
class IntegerType;
typedef IntegerType<int> Integer;

template <class T>
class Integer32Type;
typedef Integer32Type<int32_t> Integer32;

template <class T>
class Integer64Type;
typedef Integer64Type<int64_t> Integer64;

class String;
class Table;

template <class T>
class UnsignedIntegerType;
typedef UnsignedIntegerType<unsigned int> UnsignedInteger;

template <class T>
class UnsignedInteger32Type;
typedef UnsignedInteger32Type<uint32_t> UnsignedInteger32;

template <class T>
class UnsignedInteger64Type;
typedef UnsignedInteger64Type<uint64_t> UnsignedInteger64;

template <class T>
class UnsignedLongType;
typedef UnsignedLongType<uint64_t> UnsignedLong;

template <class T>
class UnsignedShortType;
typedef UnsignedShortType<unsigned short> UnsignedShort;

template <class T>
class Vector;
}


namespace swatch {

namespace core {
namespace rules {
class NoEmptyCells;
}
}

namespace action {

class File;

/**
 * @brief  Class for undefined default validator of type T.
 *
 * @tparam T     Type of the parameter class.
 */
template <typename T>
class UndefinedDefaultRule : public core::Rule<T> {
  // Triggered the following assert only if the class is instantiated
  BOOST_STATIC_ASSERT_MSG(sizeof(T) == 0, "No default validator defined for class T");

public:
  virtual bool apply(const T& aValue) const final
  {
    return false;
  }

private:
  virtual void describe(std::ostream& aStream) const final {}
};
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
template <typename T, class Enable = void>
struct DefaultCmdParRule {
  typedef UndefinedDefaultRule<T> type;
};
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
// clang-format off
template<typename T>
struct DefaultCmdParRule< T, typename std::enable_if<
    std::is_same<xdata::Boolean, T>{} ||
    std::is_same<xdata::Integer, T>{} ||
    std::is_same<xdata::Integer32, T>{} ||
    std::is_same<xdata::Integer64, T>{} ||
    std::is_same<xdata::UnsignedInteger, T>{} ||
    std::is_same<xdata::UnsignedInteger32, T>{} ||
    std::is_same<xdata::UnsignedInteger64, T>{} ||
    std::is_same<xdata::UnsignedShort, T>{} ||
    std::is_same<xdata::UnsignedLong, T>{} ||
    std::is_same<xdata::Float, T>{} ||
    std::is_same<xdata::Double, T>{} ||
    std::is_same<float, T>{} ||
    std::is_same<double, T>{}
  >::type > {

  typedef core::rules::FiniteNumber<T> type;
};
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
template<typename T>
struct DefaultCmdParRule< T, typename std::enable_if<
    std::is_same<xdata::Vector<xdata::Boolean>, T>{} ||
    std::is_same<xdata::Vector<xdata::Integer>, T>{} ||
    std::is_same<xdata::Vector<xdata::Integer32>, T>{} ||
    std::is_same<xdata::Vector<xdata::Integer64>, T>{} ||
    std::is_same<xdata::Vector<xdata::UnsignedInteger>, T>{} ||
    std::is_same<xdata::Vector<xdata::UnsignedInteger32>, T>{} ||
    std::is_same<xdata::Vector<xdata::UnsignedInteger64>, T>{} ||
    std::is_same<xdata::Vector<xdata::UnsignedShort>, T>{} ||
    std::is_same<xdata::Vector<xdata::UnsignedLong>, T>{} ||
    std::is_same<xdata::Vector<xdata::Float>, T>{} ||
    std::is_same<xdata::Vector<xdata::Double>, T>{} ||
    std::is_same<std::vector<float>, T>{} ||
    std::is_same<std::vector<double>, T>{}
  >::type > {

  typedef core::rules::FiniteVector<T> type;
};
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
template<typename T>
struct DefaultCmdParRule< T, typename std::enable_if<
    std::is_same<xdata::String, T>{} ||
    std::is_same<bool, T>{} ||
    std::is_same<int32_t, T>{} ||
    std::is_same<int64_t, T>{} ||
    std::is_same<uint32_t, T>{} ||
    std::is_same<uint64_t, T>{} ||
    std::is_same<std::string, T>{} ||
    std::is_same<std::vector<bool>, T>{} ||
    std::is_same<std::vector<int32_t>, T>{} ||
    std::is_same<std::vector<int64_t>, T>{} ||
    std::is_same<std::vector<uint32_t>, T>{} ||
    std::is_same<std::vector<uint64_t>, T>{} ||
    std::is_same<std::vector<std::string>, T>{} ||
    std::is_same<swatch::action::File, T>{}
  >::type > {

  typedef core::rules::None<T> type;
};
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
template<typename T>
struct DefaultCmdParRule< T, typename std::enable_if<
    std::is_same<xdata::Table, T>{}
  >::type > {

  typedef core::rules::NoEmptyCells type;
};
// clang-format off
// ----------------------------------------------------------------------------


} // namespace action
} // namespace swatch


#endif /* __SWATCH_ACTION_DEFAULTCOMMANDPARRULES_HPP__ */
