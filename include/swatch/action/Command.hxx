/*
 * File:   Command.hxx
 * Author: kreczko
 *
 * Created on March 3, 2015
 */

#ifndef __SWATCH_ACTION_COMMAND_HXX__
#define __SWATCH_ACTION_COMMAND_HXX__


// IWYU pragma: private, include "swatch/action/Command.hpp"

#include "swatch/action/ActionableObject.hpp"
#include "swatch/action/Command.hpp"
#include "swatch/core/PSetConstraint.hpp"
#include "swatch/core/Rule.hpp"


namespace swatch {
namespace action {


// ----------------------------------------------------------------------------
// TODO (when migrated to full C++11) : Call other Command constructor to minimise duplication
template <typename T>
Command::Command(const std::string& aId, ActionableObject& aResource, const T& aDefault) :
  ObjectFunctionoid(aId, "", aResource),
  mActionableStatus(aResource.mStatus),
  mDefaultResult(T(aDefault)),
  mState(kInitial),
  mProgress(0.),
  mStatusMsg("initialised"),
  mResult(new boost::any()),
  mMutex()
{
#ifndef SWATCH_NO_XDATA
  // T must be derived from xdata::Serializable
  BOOST_STATIC_ASSERT((boost::is_base_of<xdata::Serializable, T>::value));
#endif
}


// ----------------------------------------------------------------------------
template <typename T>
Command::Command(const std::string& aId, const std::string& aAlias, ActionableObject& aResource, const T& aDefault) :
  ObjectFunctionoid(aId, aAlias, aResource),
  mActionableStatus(aResource.mStatus),
  mDefaultResult(T(aDefault)),
  mState(kInitial),
  mProgress(0.),
  mStatusMsg("initialised"),
  mResult(new boost::any()),
  mMutex()
{
#ifndef SWATCH_NO_XDATA
  // T must be derived from xdata::Serializable
  BOOST_STATIC_ASSERT((boost::is_base_of<xdata::Serializable, T>::value));
#endif
}


// ----------------------------------------------------------------------------
template <typename T, typename R>
void Command::registerParameter(const std::string& aName, const T& aDefaultValue, R aRule)
{
#ifndef SWATCH_NO_XDATA
  // T must be derived from xdata::Serializable
  BOOST_STATIC_ASSERT_MSG((boost::is_base_of<xdata::Serializable, T>::value), "class T must be a descendant of xdata::Serializable");
#endif

  // R must be derived from Rule<T>
  BOOST_STATIC_ASSERT_MSG((std::is_base_of<core::Rule<T>, R>::value), "class R must be a descendant of core::Rule<T>");

  if (getPath() != getId()) {
    // The Command has already been registered. Parameters list cannot be modified
    SWATCH_THROW(CommandParameterRegistrationFailed("Registering parameter outside constructors is not allowed"));
  }

  if (mDefaultParams.has(aName)) {
    std::ostringstream lExc;
    lExc << "Parameter " << aName << " is already registered";
    SWATCH_THROW(CommandParameterRegistrationFailed(lExc.str()));
  }

  core::Match lResult = aRule(aDefaultValue);
  if (!lResult.ok) {
    std::ostringstream lExc;
    lExc << "Command " << getId() << " - "
         << "The default value for parameter '" << aName << "'" //" (" << aDefaultValue << ")"
         << " does not comply with rule '" << aRule << "'";
    SWATCH_THROW(CommandParameterCheckFailed(lExc.str()));
  }

  mDefaultParams.add(aName, aDefaultValue);

  mRules[aName] = std::shared_ptr<core::AbstractRule>(new R(aRule));
}


// ----------------------------------------------------------------------------
template <typename C>
void Command::addConstraint(const std::string& aName, const C& aConstraint)
{

  if (getPath() != getId()) {
    // The Command has already been registered. Parameters list cannot be modified
    SWATCH_THROW(CommandConstraintRegistrationFailed("Adding constraints outside constructors is not allowed"));
  }

  if (mConstraints.count(aName)) {
    std::ostringstream lExc;
    lExc << "Command " << getId() << " - "
         << "Constraint " << aName << " is already registered";
    SWATCH_THROW(CommandConstraintRegistrationFailed(lExc.str()));
  }

  core::Match lMatch = false;
  try {
    lMatch = aConstraint(mDefaultParams);
  }
  catch (const CommandParameterCheckFailed& lCheckFailed) {
    std::ostringstream lExc;
    lExc << "Command " << getId() << " - "
         << "Default parameters do not satisfy the requirement for constraint "
         << aName << "[" << aConstraint << "]"
         << std::endl;
    lExc << lCheckFailed.what();
    SWATCH_THROW(CommandConstraintRegistrationFailed(lExc.str()));
  }

  if (!lMatch.ok) {
    // Complain here
    std::ostringstream lExc;
    lExc << "Default parameters do not comply with constraint " << aName << "[" << aConstraint << "]";
    SWATCH_THROW(CommandConstraintRegistrationFailed(lExc.str()));
  }

  mConstraints[aName] = std::shared_ptr<C>(new C(aConstraint));
}


template <typename T>
void Command::setResult(const T& aResult)
{
  boost::unique_lock<boost::mutex> lock(mMutex);
  mResult.reset(new boost::any(aResult));
}


// ----------------------------------------------------------------------------
template <typename T>
void Command::addExecutionDetails(const std::string& aId, const T& aInfoItem)
{
#ifndef SWATCH_NO_XDATA
  // T must be derived from xdata::Serializable
  BOOST_STATIC_ASSERT_MSG((boost::is_base_of<xdata::Serializable, T>::value), "class T must be a descendant of xdata::Serializable");
#endif

  boost::unique_lock<boost::mutex> lLock(mMutex);
  if (mExecutionDetails.has(aId))
    mExecutionDetails.erase(aId);
  mExecutionDetails.add<T>(aId, aInfoItem);
}


template <typename T>
const T& CommandSnapshot::getResult() const
{
  if (mResult->empty())
    SWATCH_THROW(core::RuntimeError("Cannot cast result to type " + core::demangleName(typeid(T).name()) + " as no result exists"));

  const T* lResult = boost::any_cast<T>(mResult.get());
  if (lResult == NULL)
    SWATCH_THROW(core::RuntimeError("Cannot cast result of type " + core::demangleName(typeid(mResult->type()).name()) + " to " + core::demangleName(typeid(T).name())));
  return *lResult;
}


} // namespace action
} // namespace swatch

#endif /* __SWATCH_ACTION_COMMAND_HXX__ */
