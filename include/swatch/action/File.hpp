
#ifndef __SWATCH_ACTION_FILE_HPP__
#define __SWATCH_ACTION_FILE_HPP__


// Standard headers
#include <string>


namespace swatch {
namespace action {


//! Simple class used to represent parameters that are files (e.g. bitfiles, or archives containing bitfiles)
class File {
public:
  File(const std::string& aPath, const std::string& aName);
  ~File();

  const std::string& getPath() const;

  const std::string& getName() const;

private:
  std::string mPath;
  std::string mName;
};


}
}

#endif /* __SWATCH_ACTION_FILE_HPP__ */
