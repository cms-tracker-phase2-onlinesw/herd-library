#include "swatch/core/exception.hpp"


namespace swatch {
namespace core {


// Exception::Exception() throw() :
//   mThreadId(std::this_thread::get_id())
// {
// }


Exception::Exception(const std::string& aMessage) :
  mMessage(aMessage)
{
}


Exception::~Exception() throw()
{
}


const char*
Exception::what() const throw()
{
  return mMessage.c_str();
}


} // namespace core
} // namespace swatch