
#include "swatch/action/File.hpp"


namespace swatch {
namespace action {


File::File(const std::string& aPath, const std::string& aName) :
  mPath(aPath),
  mName(aName)
{
}


File::~File()
{
}


const std::string& File::getPath() const
{
  return mPath;
}


const std::string& File::getName() const
{
  return mName;
}


} // namespace action
} // namespace swatch
