# HERD library

The HERD library is used to support the development of on-board software applications for controlling and monitoring ATCA boards in the phase-2 upgrade of the CMS tracker. Specifically, it provides a framework for the registration of arbitrary system-/board-specific control/configuration procedures and monitoring data, which can then be accessed from generic system-/board-independent applications using the library's abstract interfaces. This is achieved by users creating plugin libraries that declare board-/system-specific control procedures; these plugins are then loaded by a generic board-/system-independent control application in order to control electronics boards remotely over the network. The HERD library is a minimal fork of the SWATCH framework that was developed for the phase-1 upgrade of the CMS level-1 trigger system.

Related repositories:

 * The *herd-dummy* repository implements an example plugin library. It illustrates how to create a plugin library that registers some HERD commands, and 1 finite state machine.
 * The *herd-control-app* repository implements the HERD control application, whose network interface allows remote applications to run HERD commands and FSM transitions.


## Dependencies

The main dependencies of the HERD library are:

 * Build utilities: make, CMake3
 * [boost](https://boost.org)
 * [log4cplus](https://github.com/log4cplus/log4cplus)


## Build instructions

 1. Install the dependencies listed above. For example, on CentOS7:
```
yum install gcc-c++ make cmake3 boost-devel log4cplus-devel
```

 2. Build the HERD library
```
mkdir build
cd build
cmake3 ..
make
```

Then, if you want to install the library (and header files), just run:
```
sudo make install
```


## Test suite

The functionality of the library's core classes and functions are validated by an extensive suite of unit tests (implemented using the boost test framework, in the `tests` directory). This test suite can be run as follows:
```
source tests/env.sh
./build/tests/herd-test-runner --log_level=test_suite
```
