
get_filename_component(herd_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

find_package(Boost 1.53.0 REQUIRED COMPONENTS chrono filesystem regex thread unit_test_framework)

if(NOT TARGET HERD::herd)
    include("${herd_CMAKE_DIR}/herdTargets.cmake")
endif()
