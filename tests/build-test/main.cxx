
#include <iostream>

#include "swatch/action/Board.hpp"
#include "swatch/action/DeviceStub.hpp"


int main()
{
  std::cout << "Creating empty instance of Board class (minimal test to test build working) ..." << std::endl;

  swatch::action::Board board({});

  std::cout << " ... done (board object @ " << &board << ")" << std::endl;

  return 0;
}
