#include <boost/test/unit_test.hpp>

// SWATCH headers
#include "swatch/action/File.hpp"


namespace swatch {
namespace action {
namespace test {


BOOST_AUTO_TEST_SUITE(FileTestSuite)

BOOST_AUTO_TEST_CASE(TestGetters)
{
  File lFile("/path/to/someFile.tgz", "A name");

  BOOST_CHECK_EQUAL(lFile.getPath(), "/path/to/someFile.tgz");
  BOOST_CHECK_EQUAL(lFile.getName(), "A name");
}

BOOST_AUTO_TEST_SUITE_END()


} /* namespace test */
} /* namespace action */
} /* namespace swatch */
